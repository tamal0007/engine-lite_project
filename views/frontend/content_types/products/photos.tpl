<div id="products_photos" class="clearfix">
	<div style="width: 665px;height:425px">
		{foreach $photo_gallery_images[1]->images as $key=>$photo}
			<a class="main_image" id="gallery_img_{$key}" href="{$photo_gallery_images[2]->images[$key]}" data-fancybox="gallery" {if $key!=0} style="display: none" {/if}>
				<img src="{$photo}" height="{$photo_gallery_images[1]->height}" width="{$photo_gallery_images[1]->width}" />
			</a>
		{/foreach}
	</div>
	<div style="width:341px;margin-left: 170px" class="owl-carousel">
		{foreach $photo_gallery_images[0]->images as $key => $photo}
			<a  class="item" id="thumb_{$key}">
				<img class="thumb" src="{$photo}" height="{$photo_gallery_images[0]->height}" width="{$photo_gallery_images[0]->width}"/>
			</a>
		{/foreach}
	</div>
</div>


<script>
	$(function(){
		var items = $('#products_photos .item');
		$('#products_photos .owl-carousel').owlCarousel({
			loop: false,
			items: 1,
			dots: false,
			nav: (items.length == 1 ? false : true),
			navText: ["❮", "❯"],
			lazyLoad: true,
			autoplay: false,
			autoplayTimeout: 5000,
			autoplayHoverPause: true,
			smartSpeed: 1000,
			autoHeight:true,
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 3
				},
				1000: {
					items: {$thumb_count}
				}
			},

		});
	});

	$(function() {
		$('a.item').click(function (e) {
			e.preventDefault();
			id = this.id.replace(/^thumb_/, '');
			$('.main_image').hide();
			$('#gallery_img_' + id).show();
			$('a.item').removeClass('active');
			$(this).addClass('active');
		});
	});

</script>

